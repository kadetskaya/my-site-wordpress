<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'my-site' );

/** MySQL database username */
define( 'DB_USER', 'wizard' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wizard' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'r_`S-7SDUh);3)rxO{24P}OST?q@O)z$5{aTGF_<XuRqy]-F>i*{@mwMwW4&{eLX' );
define( 'SECURE_AUTH_KEY',  'S,pEW/ju2=rdo0ek#yDm>w^Wx~t;AX1odU)tB!`z+.V(v`vSATU{1Gy)J)@1ceIg' );
define( 'LOGGED_IN_KEY',    'IEdOf{!x(EM-;a4597H?lTwn*ma4<DsX@f!V{Aa1.^?fRXyKO]uJ~@8uv]6B9~D|' );
define( 'NONCE_KEY',        'rs8q?.Js5R0v;m`){((<;-A^Y:Ro`a*-+Ebe QL4 .{M{_g8`Q3 ?^pWnwf[h|49' );
define( 'AUTH_SALT',        'Co^A?lyX$a@q7Yyte*H^sOjEYc$Qd~U?[D#[bDes5SAr>[YC{OzD/LqL*VBeN]|U' );
define( 'SECURE_AUTH_SALT', '{zmx?7}1U?rdR%DaB<hbx4yXAz[E8#Z?vT;1#psCn15W~Phis;3-50Kb.I#>P923' );
define( 'LOGGED_IN_SALT',   'dn{o]74(tB9Ett,Y16jk0*~l!7(c0:2ur0Q=FVw|PQdG}y(jj$u$GAy)h &j.cb,' );
define( 'NONCE_SALT',       'wi(&F@J|R!zVp{xhVU~!xr3U<|%4,Xy[H!o(iLz756W98,z3`zk~E4US@pl-|_T~' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
